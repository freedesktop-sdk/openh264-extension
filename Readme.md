Flatpak OpenH264 extension
===========================
This extension provides bootstrapping mechanism which allows installing Cisco's
free to use OpenH264 codec on end-user machine. This system is intended to be
used in conjunction with noopenh264. Applications link against stubs in noopenh264
and as such it determines the ABI. Applications however use openh264 during
runtime so the two must be ABI-compatible.

Updating
---------------------------
When there is a new tag in https://github.com/cisco/openh264, start with
updating project_version in https://gitlab.com/freedesktop-sdk/noopenh264/-/blob/master/meson.build to match the openh264 version and syncing headers to https://gitlab.com/freedesktop-sdk/noopenh264/-/tree/master/codec/api.

You will then need to update https://gitlab.com/freedesktop-sdk/openh264-extension/-/blob/master/elements/config.yml to point to new executables. Update the VERSION in https://gitlab.com/freedesktop-sdk/openh264-extension/-/blob/master/Makefile. Add a new release tag in https://gitlab.com/freedesktop-sdk/openh264-extension/-/blob/master/files/org.freedesktop.Platform.openh264.appdata.xml to set the version number in the appstream metadata, which is also used by flatpak to display the extension version. Create a tag so that the extension may land in Flathub.

To update freedesktop-sdk, first update https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/master/elements/components/noopenh264.bst to the latest noopenh264. Update openh264-version in https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/master/include/repo_branches.yml. This will ensure the new openh264 extension is properly mounted. 

To update gnome-build-meta, first update https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/master/elements/sdk/noopenh264.bst to the latest noopenh264. Update gnome-openh264-version in https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/master/elements/flatpak/gnome-openh264-version.yml. Finally, after the changes have landed in gnome-build-meta master, navigate to the most recent pipeline on the master branch and trigger the manual CI job named openh264-x86_64.

Sanity-checking installation
----------------------------
You can run `make test-extension` to run brief check that the extension downloads
Cisco openh264 as extended and the output is not corrupt

Publishing
----------
This extension is published to Flathub beta on merge and into Flathub stable on tags. These use same JWT token.
Token expected prefixes are `['org.freedesktop.Platform.openh264']` and repos `['beta', 'stable']`.

If token has expired, submit a ticket into https://github.com/flathub/flathub/ to renew.

Testing OpenH264
----------------
The release version of openh264 is installed by default. You will need to uninstall
it before testing the beta version. You can either install beta version from Flathub
beta or use following to generate it locally
```
make export-repo
flatpak remote-add --if-not-exists --user --no-gpg-verify openh264 repo
flatpak install --user -v -y openh264 org.freedesktop.Platform.openh264
```
After this the extension should be loaded on next restart of an application. You can
verify from /.flatpak-info inside application sandbox that correct openh264 extension
is enabled.

