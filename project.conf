name: openh264-extension

min-version: 2.0

# The path to the directory where elements are stored
#
element-path: elements

# Some source aliases
#
aliases:
  pypi: https://files.pythonhosted.org/packages/
  fdsdk_git: https://gitlab.com/freedesktop-sdk/
  github: https://github.com/
  (@): freedesktop-sdk.bst:include/_private/aliases.yml

(@):
- freedesktop-sdk.bst:include/runtime.yml
- freedesktop-sdk.bst:include/_private/mirrors.yml

plugins:
- origin: junction
  junction: plugins/buildstream-plugins.bst
  elements:
  - cmake
  - make
  - meson
  sources:
  - patch

- origin: junction
  junction: plugins/bst-plugins-experimental.bst
  elements:
  - flatpak_image
  - flatpak_repo
  sources:
  - git_repo

# Define some variables to get things installed into the
# correct places
#
variables:
  prefix: "/usr/lib/%{gcc_triplet}/openh264"
  flatpak_xmldir: "%{datadir}/app-info/xmls"
  lib: "lib"
  bindir: "/usr/bin"
  ldflags_defaults: "%{common_ldflags} -L %{libdir} %{local_ldflags}"

options:
  arch:
    type: arch
    description: Architecture
    variable: arch
    values:
    - x86_64
    - aarch64

  bootstrap_arch:
    type: arch
    description: Binary seed architecture
    variable: bootstrap_arch
    values:
    - aarch64
    - x86_64

  release_kind:
    type: enum
    description: Determines whether to append beta suffix to ref
    values:
    - stable
    - beta
    default: beta

sandbox:
  build-arch: '%{arch}'

artifacts:
- url: https://cache.freedesktop-sdk.io:11001

source-caches:
- url: https://cache.freedesktop-sdk.io:11001
