#!/usr/bin/env python3
"""Usage python validate_repo.py --path /path/to/ostree/repo"""

import subprocess
import os
import argparse


def list_refs(path):
    refs = subprocess.check_output(["ostree", "refs"], cwd=path).decode().splitlines()
    return set(refs)


def validate_environment():
    if not os.environ.get("GITLAB_CI", "false") == "true":
        raise ValueError("Not running inside Gitlab CI")

    if not os.environ.get("CI_COMMIT_REF_PROTECTED", "false") == "true":
        raise ValueError("Not running for a protected ref")


def validate_refs(path):
    is_tag = os.environ.get("CI_COMMIT_TAG")
    is_master_branch = os.environ.get("CI_COMMIT_BRANCH") == "master"
    release_channel = os.environ.get("RELEASE_CHANNEL", None)

    if is_tag and release_channel != "stable":
        raise ValueError("RELEASE_CHANNEL is not set to stable on tags")

    if is_master_branch and release_channel != "beta":
        raise ValueError("RELEASE_CHANNEL is not set to beta on master branch")

    expected_refs = {
        "runtime/org.freedesktop.Platform.openh264/aarch64",
        "runtime/org.freedesktop.Platform.openh264/x86_64",
    }

    refs = list_refs(path)
    refs_unbranched = {item.rsplit("/", 1)[0] for item in refs}
    if not refs:
        raise ValueError(f"No refs found in {path}")

    for ref in refs:
        if ref.startswith(("appstream/", "appstream2/", "screenshots/")):
            continue

        ref_splits = ref.split("/")
        ref_branch = ref_splits[3]

        if len(ref_splits) != 4:
            raise ValueError(f"Invalid ref: {ref}")

        if is_master_branch and not ref_branch.endswith("beta"):
            raise ValueError(f"Ref branch does not end in beta on master branch: {ref}")

        if is_tag and ref_branch.endswith("beta"):
            raise ValueError(f"Ref branch ends with beta on tag: {ref}")

    if not refs_unbranched == expected_refs:
        raise Exception(f"Mismatch with expected refs. Found: {refs}")


def main():
    parser = argparse.ArgumentParser(
        description="Validate refs before Flathub push (mesa-git)"
    )
    parser.add_argument("--path", type=str, help="path to the OSTree repository")
    args = parser.parse_args()

    validate_environment()
    validate_refs(args.path)


if __name__ == "__main__":
    main()
