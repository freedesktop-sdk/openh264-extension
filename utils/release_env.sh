#!/bin/bash
export RELEASE_CHANNEL=beta
export REPO_TOKEN="${FLATHUB_REPO_TOKEN}"
if [ -n "${CI_COMMIT_TAG}" ]; then
  export RELEASE_CHANNEL=stable
fi
