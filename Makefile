SHELL=/bin/bash
RELEASE_KIND?=beta
ARCH?=$(shell uname -m | sed "s/^i.86$$/i686/" | sed "s/^ppc/powerpc/")
BOOTSTRAP_ARCH?=$(shell uname -m | sed "s/^i.86$$/i686/")
REPO?=repo
ARCH_OPTS=-o arch $(ARCH) -o bootstrap_arch $(BOOTSTRAP_ARCH)
BST=bst --colors $(ARCH_OPTS) -o release_kind $(RELEASE_KIND)
VERSION=2.5.0
ifeq ($(RELEASE_KIND), beta)
BRANCH=$(VERSION)beta
else
BRANCH=$(VERSION)
endif
GIT_DESCRIBE := $(shell git rev-parse HEAD)

build:
	$(BST) build flatpak-repo.bst test-extension.bst

clean-repo:
	rm -rf $(REPO)

export-repo:
	$(BST) artifact checkout flatpak-repo.bst --directory flatpak-repo.bst
	test -e $(REPO) || ostree init --repo=$(REPO) --mode=archive
	flatpak build-commit-from --src-repo=flatpak-repo.bst --subject $(GIT_DESCRIBE) $(REPO)
	rm -rf flatpak-repo.bst

test-extension: export XDG_DATA_HOME=$(CURDIR)/temp
test-extension: export-repo
	rm -rf temp
	mkdir -p temp
	flatpak remote-add --if-not-exists --user --no-gpg-verify openh264 $(REPO)
	flatpak remote-ls openh264
	flatpak install --user -v -y openh264 org.freedesktop.Platform.openh264//$(BRANCH)
	$(BST) shell --mount ${XDG_DATA_HOME}/flatpak/runtime/org.freedesktop.Platform.openh264/$(ARCH)/$(BRANCH)/active/files/extra/ /flatpak \
	--mount /etc/resolv.conf /etc/resolv.conf test-extension.bst /bin/test-extension.sh


.PHONY: build clean-repo export test-extension
